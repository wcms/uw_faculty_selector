Faculty Selector
===

## Overview

The faculty selector module is a custom webform element that gives form authors the ability to include a picklist of faculty at the university generated from an external data source, this could be OFIS directly, or any other valid JSON data source. The selector can be configured to be dependent on another webform element to provide filtering. For example,  if the webform contains a department picklist and the faculty selector is configured to group by department with the controlling element being the department picklist, only faculty with a matching department will be shown once a selection is made on the department picklist. 

## Installation

Download and include the module in the appropriate module directory for your drupal installation or include the module in your make file

## Contributors

Scott Morrison

## Usage

### Basic Usage
1. Create webform content 
2. Select Faculty Selector as the type for one of the form elements and click add
3. Keeping the default values provided will generate the list of faculty from the marketing sandbox SFDC instance. The key for the faculty will be the salesforce id of the contact records. These records are kept in sync with the OFIS faculty data. Note that it is possible to genereate the faculty list from the OFIS webservice directly using this module 
4. View the web form and you will see the list of faculty that can be selected


### Advanced Usage

1. Create webform content 
2. Select Faculty Selector as the type for one of the form elements and click add
3. Enter which department you want to load, you can restrict the picklist to a specific department or enter All to return all departments 
4. Enter the data source, the data source should be a GET web service that returns valid JSON for example the SFDC service ( https://devpro-uow.cs3.force.com/facultylist ) returns data as follows:
   `[{"optout":false,"name":"Test Test","id":"XXXXXXXXXXXXXXXXXX","department":"Systems Design Engineering"},{"optout":false,"name":"Test2 Test2","id":"XXXXXXXXXXXXXXXXXX","department":"Management Sciences"}]`
5. Enter the JSON key that will be used to populate the value attribute in the select options into Value Field
6. Enter the JSON key that will be used to populate the label for the select options into the Label Field
7. Enter the JSON key that will be used to group options (this is important when using a controlling field, if there is no controlling field this has no effect)
8. Enter the key of the field that will control which group of options to show, this fields values should correspond with the values in the JSON object who's key is selected as the grouping key.
9. View the form, you should now see the faculty selector, and changing the controlling fields value will change which options are available in the faculty selector
