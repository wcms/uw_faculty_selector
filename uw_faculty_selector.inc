<?php

/**
 * @file
 * Provides the implmentation for the faculty selector custom webform input.
 */

/**
 *
 */
function _webform_defaults_fselector() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'department' => 'All',
      'controller' => '',
      'source' => 'https://devpro-uow.cs3.force.com/facultylist',
      'value_field' => 'id',
      'label_field' => 'name',
      'group_field' => 'department',
    ),
  );
}

/**
 *
 */
function _webform_edit_fselector($component) {
  $form = array();
  $form['extra']['department'] = array(
    '#type' => 'textfield',
    '#title' => t('Department'),
    '#default_value' => $component['extra']['department'],
    '#size' => 60,
    '#required' => FALSE,
    '#maxlength' => 255,
    '#description' => t('The faculty to load'),
  );

  $form['extra']['source'] = array(
    '#type' => 'textfield',
    '#title' => t('Source'),
    '#default_value' => $component['extra']['source'],
    '#size' => 60,
    '#required' => FALSE,
    '#maxlength' => 255,
    '#description' => t('The public REST service to pull the faculty from'),
    '#element_validate' => array('_webform_edit_url_validate'),
  );

  $form['extra']['value_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Value Field'),
    '#default_value' => $component['extra']['value_field'],
    '#size' => 60,
    '#required' => FALSE,
    '#maxlength' => 255,
    '#description' => t('The field on the JSON object to pull the option value from'),
  );

  $form['extra']['label_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Label Field'),
    '#default_value' => $component['extra']['label_field'],
    '#size' => 60,
    '#required' => FALSE,
    '#maxlength' => 255,
    '#description' => t('The field on the JSON object to pull the option label from'),
  );

  $form['extra']['group_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Field'),
    '#default_value' => $component['extra']['group_field'],
    '#size' => 60,
    '#required' => FALSE,
    '#maxlength' => 255,
    '#description' => t('The field on the JSON object to group options'),
  );

  $form['extra']['controller'] = array(
    '#type' => 'textfield',
    '#title' => t('Controlling Field'),
    '#default_value' => $component['extra']['controller'],
    '#size' => 60,
    '#required' => FALSE,
    '#maxlength' => 255,
    '#description' => t('The key of the field who\'s value controls the select options'),
  );

  return $form;
}

/**
 *
 */
function _webform_render_fselector($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_URL, $component['extra']['source']);
  $faculty_response = curl_exec($curl);
  curl_close($curl);

  if ($faculty_response !== FALSE) {

    $decoded_response = json_decode($faculty_response, TRUE);

    if ($decoded_response !== NULL) {

      $options = array();
      $options[""] = "- Select -";
      foreach ($decoded_response as $key => $f) {

        $labelfields = preg_split("/;/", $component['extra']['label_field']);
        $label = "";
        foreach ($labelfields as $l) {
          $label .= $f[$l] . " ";
        }

        $options[$f[$component['extra']['value_field']]] = t($label);
      }

      drupal_add_js(drupal_get_path('module', 'uw_faculty_selector') . '/js/uw_faculty_selector.js');
      drupal_add_js('jQuery(document).ready(function(){' .
          'init_faculty_options("edit-submitted-' . preg_replace('/_+/', '-', strtolower($component['form_key'])) . '", ' . $faculty_response .
           ', "edit-submitted-' . preg_replace('/_+/', '-', strtolower($component['extra']['controller'])) . '"' .
           ', "' . $component['extra']['value_field'] . '"' .
           ', "' . $component['extra']['label_field'] . '"' .
           ', "' . $component['extra']['group_field'] . '"' .
          ')' .
      '});', 'inline');

      $faculty = array(
        '#type' => 'select',
        '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
        '#required' => $component['required'],
        '#default_value' => '',
        '#weight' => $component['weight'],
        '#options' => $options,
      );
      return $faculty;
    }
    else {
      watchdog('uw_faculty_selector', 'Invalid JSON response recieved from target', array(), WATCHDOG_ERROR);
    }

  }
  else {
    watchdog('uw_faculty_selector', 'Request to REST service failed with error: ' . curl_error($curl), array(), WATCHDOG_ERROR);
  }

}

/**
 *
 */
function _webform_edit_url_validate($element, &$form_state) {

  $value = trim($element['#value']);
  form_set_value($element, $value, $form_state);

  if (!valid_url($value, TRUE)) {
    form_error($element, check_plain(webform_filter_xss(t('!name field must be a valid url.', array('!name' => $element['#title'])))));
  }

}
