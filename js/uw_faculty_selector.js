/**
 * @file
 * The javascript to setup and controller the faculty selctor dependent picklist on the client side, dependency is optional.
 */

function init_faculty_options(form_key, options, dependent_element, value_field, label_field, group_field) {

  var optionsByDepartment = {};
  var allOptions = options;

  console.log(dependent_element);

  var dependent_element = jQuery('#' + dependent_element);
  var faculty_selector = jQuery('#' + form_key);

  var label_fields = label_field.split(';');

  faculty_selector.append('<option>- Select -</option>');
  jQuery.each(options, function (key, option) {

    var grouping = [];

    if (jQuery.isArray(option[group_field])) {
      grouping = option[group_field];
    }
    else {
      grouping.push(option[group_field]);
    }

    jQuery.each(grouping, function (key, group) {

      if (!optionsByDepartment[group]) {
        optionsByDepartment[group] = [];
      }
      optionsByDepartment[group].push(option);

    });

    var label = '';
    for (var i = 0; i < label_fields.length; ++i) {
      label += option[label_fields[i]] + ' ';
    }

    faculty_selector.append('<option value="' + option[value_field] + '">' + label + '</option>');

  });

  if (dependent_element) {
    dependent_element.change(function () {

      faculty_selector.empty();
      faculty_selector.append('<option>- Select -</option>');

      var options = [];
      if (optionsByDepartment[jQuery(this).val()] != undefined) {
        options = optionsByDepartment[jQuery(this).val()];
      }
      else if (jQuery(this).val() == '' || jQuery(this).val() == 'Unknown') {
        options = allOptions;
      }

      jQuery.each(options, function (key, option) {
        var label = '';
        for (var i = 0; i < label_fields.length; ++i) {
          label += option[label_fields[i]] + ' ';
        }

        faculty_selector.append('<option value="' + option[value_field] + '">' + label + '</option>');
      });

    });
  }

}
